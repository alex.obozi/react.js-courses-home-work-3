import React from 'react';
import './App.css';
import Input from "./components/Input";

class App extends React.Component {

  state = {
    imgUrl: ''
  };

  previewFile = (e) => {

    const reader  = new FileReader();

    reader.readAsDataURL(e.target.files[0]);

    reader.onloadend = () => {
      console.log('test');
      this.setState({
        imgUrl: reader.result
      })
    }

  };

  render(){

    const {previewFile} = this;
    const {imgUrl} = this.state;

    return (
      <div className="App">
        <Input
          imgUrl={imgUrl}
          previewFile={previewFile}
        />
      </div>
    );
  }
  
}

export default App;
