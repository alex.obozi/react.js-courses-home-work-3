import React from 'react';

const Input = ({previewFile, imgUrl}) => {
  return (
    <>
      <label
        className={`custom-label ${imgUrl ? 'uploaded' : ' '}`}
        htmlFor='upload-photo'
        style={{backgroundImage: `url(${imgUrl})`}}
      >
        Upload your image
      </label>
      <input
        id='upload-photo'
        type='file'
        onChange={previewFile}
      />
    </>
  );
};

export default Input;
