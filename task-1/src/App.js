import React from 'react';
import './App.css';
import Chart from 'chart.js';

class App extends React.Component {

  grafRef = React.createRef();

  state = {
    chartData: [10, 0, 55, 2, 25, 45, 30]
  }

  componentDidMount() {
    let ctx = document.getElementById('myChart').getContext('2d');
    let chartData = this.state.chartData;
    this.grafRef.current.chart = new Chart(ctx, {
      // The type of chart we want to create
      type: 'line',

      // The data for our dataset
      data: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        datasets: [{
          label: 'My First dataset',
          backgroundColor: 'rgb(255, 99, 132)',
          borderColor: 'rgb(255, 99, 132)',
          data: chartData
        }]
      },

      // Configuration options go here
      options: {}
    });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    this.grafRef.current.chart.data.datasets[0].data = this.state.chartData;
    this.grafRef.current.chart.update();
  }

  handleRandom = (min, max, count = 7 ) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    let res = [];
    for( let i = 0; i < count; i++){
      res[i] = Math.floor(Math.random() * (max - min + 1)) + min;
    }
    this.setState({
      chartData: res
    });
    console.log('State has changed', this.state.chartData);
  }

  render(){
    const {handleRandom} = this;
    return (
      <div className="App">
        <canvas id="myChart" ref={this.grafRef} />
        <button
          onClick={() => handleRandom(0, 100)}
        >
          Random
        </button>
      </div>
    );

  }

}

export default App;
